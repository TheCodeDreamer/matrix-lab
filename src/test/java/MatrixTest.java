
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

class MatrixTest {
    Matrix matrix;

    @BeforeEach
    void init(){
        matrix = new Matrix(5);
    }

    @Test
    @DisplayName("Проверка на единичную матрицу")
    void isIdentyMatrix(){
        boolean isIdenty = true;
        for (int i = 0; i < 5; i++) {
            if(matrix.getElement(i,i) != 1){
                isIdenty=false;
                break;
            }
        }
        assertTrue(isIdenty);
    }

    @Test
    @DisplayName("Проверка на размер матрицы")
    void getSize() {
        assertEquals(5, matrix.getSize());
    }

    @Test
    @DisplayName("Проверка get")
    void getElement() {
        assertEquals(0,matrix.getElement(0,1));
    }

    @Test
    @DisplayName("Проверка set")
    void setElement(){
        matrix.setElement(0,0,5);
        assertEquals(5,matrix.getElement(0,0));
    }

    @Test
    @DisplayName("Проверка суммы")
    void sum() {
        Matrix second = new Matrix(5);
        Matrix result = matrix.sum(second);
        boolean isCorrect = true;
        for (int i = 0; i < 5; i++) {
            if(result.getElement(i,i) != 2){
                isCorrect=false;
                break;
            }
        }
        assertTrue(isCorrect);
    }

    @Test
    @DisplayName("Проверка умножения")
    void product() {
        Matrix second = new Matrix(5);
        for (int i = 0; i < 5; i++) {
            matrix.setElement(i,i,i+1);
            second.setElement(i,i,5-i);
        }
        Matrix result = matrix.product(second);
        Matrix expected = new Matrix(5);
        expected.setElement(0,0,5);
        expected.setElement(1,1,8);
        expected.setElement(2,2,9);
        expected.setElement(3,3,8);
        expected.setElement(4,4,5);
        assertEquals(expected.toString(),result.toString());
    }
}